package com.example.demo;

import com.example.demo.entities.Passport;
import com.example.demo.repositories.PassportRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class PassportRepositoryTest {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    PassportRepository passportRepository;

    @Test
    public void findById(){
        Passport passport = passportRepository.findById(100001L);
        Assert.assertEquals("A123", passport.getNumber());
    }

    @Test
    @DirtiesContext
    public void deleteById(){
        Passport passport = passportRepository.findById(100002L);
        Assert.assertEquals("B123", passport.getNumber());

        passportRepository.deleteById(100002L);

        Passport passportAfterDelete = passportRepository.findById(100002L);
        Assert.assertNull(passportAfterDelete);
    }

    @Test
    @DirtiesContext
    public void save_edit(){
        Passport passport = passportRepository.findById(100003L);
        Assert.assertEquals("C123", passport.getNumber());

        passport.setNumber("Test Updated");
        passportRepository.save(passport);

        Passport passport1 = passportRepository.findById(100003L);
        Assert.assertEquals("Test Updated", passport.getNumber());
    }

    @Test
    @DirtiesContext
    public void save_insert(){
        Passport passport = passportRepository.findById(1L);
        Assert.assertNull(passport);

        Passport passportToAdd = new Passport("New passport");
        passportRepository.save(passportToAdd);

        Passport passport1 = passportRepository.findById(1L);
        Assert.assertEquals("New passport", passport1.getNumber());
    }

    @Test
    public void getStudentViaPassportTest(){
        Passport passport = em.find(Passport.class, 100001L);
        logger.info("PASSPORT -> {}", passport);
        logger.info("STUDENT -> {}", passport.getStudent());
    }
}
