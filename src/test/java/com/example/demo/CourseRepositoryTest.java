package com.example.demo;

import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import com.example.demo.repositories.CourseRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.repository.history.support.RevisionEntityInformation;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class CourseRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    CourseRepository courseRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById(){
        Course course = courseRepository.findById(10001L);
        Assert.assertEquals("Testowo", course.getName());
    }

    @Test
    @DirtiesContext //adnotacja ze springa a nie hibernate (za kazdym razem odswieza)
    public void deleteById(){
        courseRepository.deleteById(10002L);
        Assert.assertNull(courseRepository.findById(10002L));
    }

    @Test
    @DirtiesContext
    public void save_edit(){
        Course course = courseRepository.findById(10001L);
        Assert.assertEquals("Testowo", course.getName());

        course.setName("Test Updated");
        courseRepository.save(course);

        Course course1 = courseRepository.findById(10001L);
        Assert.assertEquals("Test Updated", course.getName());
    }

    @Test
    @DirtiesContext
    public void save_insert(){
        Course course = courseRepository.findById(1);
        Assert.assertNull(course);

        Course courseToAdd = new Course("New course");
        courseRepository.save(courseToAdd);

        Course course1 = courseRepository.findById(1);
        Assert.assertEquals("New course", course1.getName());
    }

    @Test
    @DirtiesContext
    public void playWithEntityManager(){
        courseRepository.playWithEM();
    }

    @Test
    @Transactional
    public void getCourseWithReviews(){
        Course course = em.find(Course.class, 10001L);
        logger.info("KURS -> {}", course);
        logger.info("OCENY -> {}", course.getRv());
    }

    @Test
    @Transactional
    public void getCourseViaReview(){
        Review review = em.find(Review.class, 100005L);
        logger.info("OCENA -> {}", review);
        logger.info("KURS -> {}", review.getCourse());
    }
}