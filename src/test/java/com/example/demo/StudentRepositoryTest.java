package com.example.demo;

import com.example.demo.entities.Student;
import com.example.demo.repositories.StudentRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
@Transactional
public class StudentRepositoryTest {

    @Autowired
    EntityManager em;

    @Autowired
    StudentRepository studentRepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById(){
        Student student = studentRepository.findById(10006L);
        Assert.assertEquals("Kasia", student.getName());
    }

    @Test
    @DirtiesContext
    public void deleteById() {
        Student student = studentRepository.findById(10007L);
        Assert.assertEquals("Tomek", student.getName());

        studentRepository.deleteById(10007L);

        Student studentAfterDelete = studentRepository.findById(10007L);
        Assert.assertNull(studentAfterDelete);
    }
    @Test
    @DirtiesContext
    public void save_edit(){
        Student student = studentRepository.findById(10008L);
        Assert.assertEquals("Zuzia", student.getName());

        student.setName("Zuzia Updated");
        studentRepository.save(student);

        Student student1 = studentRepository.findById(10008L);
        Assert.assertEquals("Zuzia Updated", student.getName());
    }

    @Test
    @DirtiesContext
    public void save_insert(){
        Student student = studentRepository.findById(1L);
        Assert.assertNull(student);

        Student studentToAdd = new Student("New student Ziom");
        studentRepository.save(studentToAdd);

        Student student1 = studentRepository.findById(1L);
        Assert.assertEquals("New student Ziom", student1.getName());
    }

    @Test
    public void getStudentWithPassportTest(){
        Student student = em.find(Student.class, 10006L);
        logger.info("STUDENT -> {}", student);
        logger.info("PASSPORT -> {}", student.getPassport());
    }

    @Test
    public void getStudentWithCoursesTest(){
        Student student = em.find(Student.class, 20001L);
        logger.info("STUDENT -> {}", student);
        logger.info("KURSY -> {}", student.getCourses());
    }
}
