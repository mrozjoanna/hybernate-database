package com.example.demo;

import com.example.demo.entities.Review;
import com.example.demo.repositories.ReviewRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.persistence.EntityManager;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DemoApplication.class)
public class ReviewRepositoryTest {
    @Autowired
    EntityManager em;

    @Autowired
    ReviewRepository reviewrepository;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Test
    public void findById(){
        Review review = reviewrepository.findById(100005L);
        Assert.assertEquals("genialny", review.getRating());
    }

    @Test
    @DirtiesContext
    public void deleteById(){
        Review review = reviewrepository.findById(100006L);
        Assert.assertEquals("dobry", review.getRating());

        reviewrepository.deleteById(100006L);

        Review passportAfterDelete = reviewrepository.findById(100006L);
        Assert.assertNull(passportAfterDelete);
    }

    @Test
    @DirtiesContext
    public void save_edit(){
        Review review = reviewrepository.findById(100007L);
        Assert.assertEquals("przecietny", review.getRating());

        review.setRating("przecietny Updated");
        reviewrepository.save(review);

        Review review1 = reviewrepository.findById(100007L);
        Assert.assertEquals("przecietny Updated", review1.getRating());
    }

    @Test
    @DirtiesContext
    public void save_insert(){
        Review review = reviewrepository.findById(1L);
        Assert.assertNull(review);

        Review reviewToAdd = new Review("New rating", "description5");
        reviewrepository.save(reviewToAdd);

        Review review1 = reviewrepository.findById(1L);
        Assert.assertEquals("New rating", review1.getRating());
    }
}
