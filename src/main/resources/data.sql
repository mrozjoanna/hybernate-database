insert into Course(id, name) values(10001, 'Testowo');
insert into Course(id, name) values(10002, 'Testowo2');
insert into Course(id, name) values(10003, 'Testowo3');
insert into Course(id, name) values(10004, 'Testowo4');
insert into Course(id, name) values(10005, 'Testowo5');

insert into Passport(id, number) values (100001, 'A123');
insert into Passport(id, number) values (100002, 'B123');
insert into Passport(id, number) values (100003, 'C123');
insert into Passport(id, number) values (100004, 'D123');

insert into Student(id, name, passport_id) values(20001, 'Kasia', 100001);
insert into Student(id, name, passport_id) values(20002, 'Tomek', 100002);
insert into Student(id, name, passport_id) values(20003, 'Zuzia', 100003);
insert into Student(id, name, passport_id) values(20004, 'Maciej', 100004);

insert into Review(id, rating, description, course_id) values(100005, 'genialny', 'description1', 10001);
insert into Review(id, rating, description, course_id) values(100006, 'dobry', 'description2', 10001);
insert into Review(id, rating, description, course_id) values(100007, 'przecietny', 'description3', 10001);
insert into Review(id, rating, description, course_id) values(100008, 'tragedia', 'description4', 10001);

Insert into student_course(student_id, course_id) VALUES (20001, 10001);
Insert into student_course(student_id, course_id) VALUES (20001, 10002);
Insert into student_course(student_id, course_id) VALUES (20002, 10001);
Insert into student_course(student_id, course_id) VALUES (20001, 10003);
Insert into student_course(student_id, course_id) VALUES (20002, 10002);
Insert into student_course(student_id, course_id) VALUES (20003, 10002);
