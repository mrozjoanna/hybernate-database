package com.example.demo;

import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import com.example.demo.entities.Student;
import com.example.demo.entities.extendsDemo.EmployeeRepository;
import com.example.demo.entities.extendsDemo.FullTimeJob;
import com.example.demo.entities.extendsDemo.PartTimeJob;
import com.example.demo.repositories.CourseRepository;
import com.example.demo.repositories.ReviewRepository;
import com.example.demo.repositories.StudentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.math.BigDecimal;
import java.util.ArrayList;

@SpringBootApplication
public class DemoApplication implements CommandLineRunner {

	@Autowired
	CourseRepository courseRepository;

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	ReviewRepository reviewRepository;

	@Autowired
	EmployeeRepository employeeRepository;

	Logger logger = LoggerFactory.getLogger(this.getClass());

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
//		Course course = courseRepository.findById(10001L);
//		logger.info("Course 10001 -> {}", course);
//		studentRepository.saveStudentWithPassport();

//		ArrayList<Review> list = new ArrayList<>();
//		list.add(new Review("5", "Super!"));
//		list.add(new Review("4", "Fajnie!"));
//		reviewRepository.addReviewWithCourse(10001L, list);

//		studentRepository.saveManyStudentsWithManyCourses();

//		Student student = new Student("LLLLLLLL");
//		Course course = new Course("ZZZZZZZZZ");
//
//		studentRepository.saveStudentWithCourse(student, course);

		FullTimeJob zbyszek = new FullTimeJob("Zbyszek", new BigDecimal(4000));
		PartTimeJob kuba = new PartTimeJob("Kuba", new BigDecimal(10));

		employeeRepository.save(zbyszek);
		employeeRepository.save(kuba);
	}
}
