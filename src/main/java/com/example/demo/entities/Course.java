package com.example.demo.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@NamedQueries(value= {
        @NamedQuery(name="select_all", query = "select c from Course c"),
        @NamedQuery(name="select_all_with_test", query = "select c from Course c where c.name like 'Test%'")
})

public class Course {
    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name", nullable = false)
    public String name;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<Review> rv = new ArrayList<>();

    @ManyToMany(mappedBy = "courses")
    private List<Student> students = new ArrayList<>();

    protected Course() {
    }

    public Course(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public List<Review> getRv() {
        return rv;
    }

    public void addRv (Review rv){
        this.rv.add(rv);
    }

    public List<Student> getStudents() {
        return students;
    }

    public void addStudent(Student student){
        students.add(student);
    }

    @Override
    public String toString() {
        return "Course{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
