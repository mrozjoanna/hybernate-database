package com.example.demo.entities.extendsDemo;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class PartTimeJob extends Employee {
    private BigDecimal hourlyWage;

    protected PartTimeJob(){}

    public PartTimeJob(String name, BigDecimal hourlyWage) {
        super(name);
        this.hourlyWage = hourlyWage;
    }
}
