package com.example.demo.entities.extendsDemo;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class EmployeeRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());

    public Employee findById (long id){
        return em.find(Employee.class, id);
    }

    public void deleteById(long id){
        Employee EmployeeToDelete = findById(id);
        em.remove(EmployeeToDelete);
    }

    public Employee save(Employee employee){
        if (employee.getId() == null){
            em.persist(employee);
        } else {
            em.merge(employee);
        }
        return employee;
    }

}
