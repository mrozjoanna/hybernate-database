package com.example.demo.entities.extendsDemo;

import javax.persistence.Entity;
import java.math.BigDecimal;

@Entity
public class FullTimeJob extends Employee {

    private BigDecimal salary;

    protected FullTimeJob(){}

    public FullTimeJob(String name, BigDecimal salary){
        super(name);
        this.salary = salary;
    }

    public BigDecimal getSalary() {
        return salary;
    }

    public void setSalary(BigDecimal salary) {
        this.salary = salary;
    }
}
