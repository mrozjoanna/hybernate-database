package com.example.demo.repositories;

import com.example.demo.entities.Course;
import com.example.demo.entities.Review;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ReviewRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());


    public Review findById(long id) {
        return em.find(Review.class, id);
    }

    public void deleteById(long id){
        Review reviewToDelete = findById(id);
        em.remove(reviewToDelete);
    }

    public Review save(Review review){
        if (review.getId() == null){
            em.persist(review);
        } else {
            em.merge(review);
        }
        return review;
    }

    public void addReviewWithCourse(Long courseId, List<Review> reviews){
        Course course = em.find(Course.class, courseId);

        for (Review rv: reviews){
            course.addRv(rv);

            rv.setCourse(course);

            em.persist(rv);

        }

//        Course course = em.find(Course.class, 10001L);
//
//        Review review = new Review("5", "super");
//        Review review2 = new Review("4", "fajnie");
//
//        course.addRv(review);
//        review.setCourse(course);
//
//        course.addRv(review2);
//        review2.setCourse(course);
//
//        em.persist(review);
//        em.persist(review2);

    }
}
