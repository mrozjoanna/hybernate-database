package com.example.demo.repositories;

import com.example.demo.entities.Course;
import com.example.demo.entities.Passport;
import com.example.demo.entities.Student;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class StudentRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());


    public Student findById(long id) {
        return em.find(Student.class, id);
    }

    public void deleteById(long id){
        Student StudentToDelete = findById(id);
        em.remove(StudentToDelete);
    }

    public Student save(Student student){
        if (student.getId() == null){
            em.persist(student);
        } else {
            em.merge(student);
        }
        return student;
    }

    public void saveStudentWithPassport(){
        Passport passport = new Passport("XYZ");
        em.persist(passport);

        Student student = new Student("Logan");
        student.setPassport(passport);
    }

    public void saveManyStudentsWithManyCourses(){
        Student student = new Student("Bolek");
        Student student2 = new Student("Lolek");

        em.persist(student);
        em.persist(student2);

        Course course = new Course("TestZZZ");
        em.persist(course);

        course.addStudent(student);
        course.addStudent(student2);

        student.addCourse(course);
        student2.addCourse(course);

        em.merge(student);
        em.merge(student2);
    }

    public void saveStudentWithCourse(Student student, Course course){
        em.persist(student);
        em.persist(course);

        student.addCourse(course);
        course.addStudent(student);

        em.merge(course);
        em.merge(student);
    }
}
