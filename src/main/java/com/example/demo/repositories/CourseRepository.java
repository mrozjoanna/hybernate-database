package com.example.demo.repositories;

import com.example.demo.entities.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

@Repository
@Transactional
public class CourseRepository {
    @Autowired
    EntityManager em;

    Logger logger = LoggerFactory.getLogger(this.getClass());


    public Course findById(long id) {
        return em.find(Course.class, id);
    }

    public void deleteById(long id){
        Course courseToDelete = findById(id);
        em.remove(courseToDelete);
    }

    public Course save(Course course){
        if (course.getId() == null){
            em.persist(course);
        } else {
            em.merge(course);
        }
        return course;
    }

    public void playWithEM(){
        logger.info("Jestem w metodzie playWithEM");

        Course course = new Course("AAAAAA");
        em.persist(course);
        em.flush();
        course.setName("BBBBBB");
        em.flush();

        logger.info("Jestem w metodzie playWithEM");
    }

}
